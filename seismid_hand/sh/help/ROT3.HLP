command procedure ROT3 <zne-list> <azimuth> <inci>
======================

key: cp:3-D rot L,Q,T system

3-dimensional rotation of components Z,N,E (given by parameter
<zne-list>) into L,Q,T system.  The two angles are <azimuth>
and <inci> (angle of incidence).  The output traces get the
component names L,Q,T.  The input traces remain unchanged.


parameters:

<zne-list>  ---  parameter type: trace list
   List of input traces.  Should be Z,N,E components.  The length
   of this trace list must be 3, otherwise the command procedure
   is aborted.  Pleas note that the order of the traces in the
   list <zne-list> must be Z,N,E (this is not checked).

<azimuth>, <inci>  ---  parameter type: real
   Rotation angles in degrees.  The rotation matrix is given in
   the HELP of the ROT command.


Example:

   ROT3 1-3 23.3 9.8   ! rotates the first three traces (succession
                       ! of components must be Z,N,E) by angles of
                       ! 23.3 deg (azimuth) and 9.8 deb (angle of
                       ! incidence)

